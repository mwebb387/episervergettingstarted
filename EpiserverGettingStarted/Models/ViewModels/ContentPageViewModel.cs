﻿using EPiServer.Core;
using EpiserverGettingStarted.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EpiserverGettingStarted.Models.ViewModels
{
    public class ContentPageViewModel : IPageViewModel<ContentPage>
    {
        public ContentPageViewModel(ContentPage currentPage)
        {
            CurrentPage = currentPage;
        }

        public ContentPage CurrentPage { get; set; }
        public IContent Section { get; set; }
    }
}