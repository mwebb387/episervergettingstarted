﻿using EPiServer.Core;
using EpiserverGettingStarted.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiserverGettingStarted.Models.ViewModels
{
    public interface IPageViewModel<T> where T : SitePageData
    {
        T CurrentPage { get; set; }
        IContent Section { get; set; }
    }
}
