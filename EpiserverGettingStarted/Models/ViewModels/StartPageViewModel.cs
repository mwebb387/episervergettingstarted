﻿using EPiServer.Core;
using EpiserverGettingStarted.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EpiserverGettingStarted.Models.ViewModels
{
    public class StartPageViewModel : IPageViewModel<StartPage>
    {
        public StartPageViewModel(StartPage currentPage)
        {
            CurrentPage = currentPage;
        }

        public StartPage CurrentPage { get; set; }
        public IContent Section { get; set; }
    }
}