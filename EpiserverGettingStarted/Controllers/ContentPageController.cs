﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using EpiserverGettingStarted.Models.Pages;
using EpiserverGettingStarted.Models.ViewModels;

namespace EpiserverGettingStarted.Controllers
{
    public class ContentPageController : PageControllerBase<ContentPage>
    {
        public ActionResult Index(ContentPage currentPage)
        {
            /* Implementation of action. You can create your own view model class that you pass to the view or
             * you can pass the page type for simpler templates */

            ContentPageViewModel model = new ContentPageViewModel(currentPage);
            return View(model);
        }
    }
}